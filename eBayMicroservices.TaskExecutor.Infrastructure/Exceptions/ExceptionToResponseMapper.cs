﻿using System;
using System.Net;
using Convey.WebApi.Exceptions;
using eBayMicroservices.TaskExecutor.Application.Exceptions;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Abstract;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Exceptions
{
    public class ExceptionToResponseMapper: IExceptionToResponseMapper
    {

        public ExceptionResponse Map(Exception exception)
            => exception switch
            {
                DomainException ex => new ExceptionResponse(new {code = ex.Code, reason = ex.Message},
                    HttpStatusCode.BadRequest),
                AppException ex => ex switch
                {
                    InvalidMarketplaceException _ => new ExceptionResponse(new {code = ex.Code, reason = ex.Message},
                        HttpStatusCode.Conflict),
                    _ => new ExceptionResponse(new {code = ex.Code, reason = ex.Message},
                    HttpStatusCode.BadRequest)
                },
                UnauthorisedUserException ex => new ExceptionResponse(new {code = ex.Code},HttpStatusCode.Unauthorized),
                _ => new ExceptionResponse(new {code = "error", reason = "There was an error."},
                    HttpStatusCode.BadRequest)
            };
    }
}