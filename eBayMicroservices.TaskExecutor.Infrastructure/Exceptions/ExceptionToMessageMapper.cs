﻿using System;
using Convey.MessageBrokers.RabbitMQ;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Exceptions
{
    public class ExceptionToMessageMapper : IExceptionToMessageMapper
    {
        public object Map(Exception exception, object message)
            => exception switch
            {
                //InvalidAggregateIdException ex => new AddOrderRejected(ex.Id,ex.Message,ex.Code),
                _ => null
            };
    }
}