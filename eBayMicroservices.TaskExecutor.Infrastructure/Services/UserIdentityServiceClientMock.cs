﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.HTTP;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Application.Services.Request;
using eBayMicroservices.TaskExecutor.Application.Services.Response;
using eBayMicroservices.TaskExecutor.Infrastructure.Services.User;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Services
{
    public class UserIdentityServiceClientMock : IUserIdentityServiceClient
    {
        private readonly IHttpClient _httpClient;
        private readonly string _url;

        public UserIdentityServiceClientMock(IHttpClient httpClient, HttpClientOptions options)
        {
            _httpClient = httpClient;
            _url = options.Services["identity"];
        }

        public async Task<GetUserIdentityResponse> GetUserIdentity(GetUserIdentityRequest request)
            => ConvertUserDtoToGetUserIdentity(
                await _httpClient.GetAsync<UserDto>($"{_url}/api/identity/{request.Id}"));

        public async Task<IEnumerable<MarketplaceDto>> GetMarketplaces()
            => ConvertMarketplaceDtoToGetMarketplaces(
                await _httpClient.GetAsync<IEnumerable<MarketplaceDto>>($"{_url}/api/marketplaces"));

        private GetUserIdentityResponse ConvertUserDtoToGetUserIdentity(UserDto userDto)
            => userDto == null
                ? new GetUserIdentityResponse()
                : new GetUserIdentityResponse(
                    userDto.Login, 
                    userDto.Name, 
                    userDto.Login, 
                    userDto.Login);
        
        private IEnumerable<MarketplaceDto> ConvertMarketplaceDtoToGetMarketplaces(
            IEnumerable<MarketplaceDto> marketplaces)
            => new List<MarketplaceDto>
            {
                new MarketplaceDto
                {
                    Name = "PL",
                    Code = "EBAY_PL",
                }
            };
    }
}