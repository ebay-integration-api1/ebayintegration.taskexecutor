﻿using System;
using System.Collections.Generic;
using System.Linq;
using eBayMicroservices.TaskExecutor.Application.Services;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Services
{
    public class GuidsFromMessageExpander : IGuidsFromMessageExpander
    {
        public IEnumerable<Guid> ExpandGuidsFromMessage(string messageContent)
            => string.IsNullOrWhiteSpace(messageContent)
                ? Enumerable.Empty<Guid>()
                : messageContent.Split(',').Select(Guid.Parse);
    }
}