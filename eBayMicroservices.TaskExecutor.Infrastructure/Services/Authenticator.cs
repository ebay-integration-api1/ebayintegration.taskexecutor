﻿using System;
using eBayMicroservices.TaskExecutor.Application.Exceptions;
using eBayMicroservices.TaskExecutor.Application.Services;
using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Services
{
    public class Authenticator : IAuthenticator
    {
        private readonly IIdentityProvider _identityProvider;

        public Authenticator(IIdentityProvider identityProvider)
        {
            _identityProvider = identityProvider;
        }
        public void Authenticate(HttpContext ctx)
        {
            string identity = _identityProvider.GetIdentityAsString(ctx);

            if (!Guid.TryParse(identity, out Guid _))
            {
                throw new UnauthorisedUserException();
            }
        }
    }
}