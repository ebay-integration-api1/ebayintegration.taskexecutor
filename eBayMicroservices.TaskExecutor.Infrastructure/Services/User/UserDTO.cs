﻿using System;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Services.User
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string EshopName { get; set; }
    }
}