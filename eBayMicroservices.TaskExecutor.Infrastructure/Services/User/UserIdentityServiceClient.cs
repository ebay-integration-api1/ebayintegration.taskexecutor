﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.HTTP;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Application.Services.Request;
using eBayMicroservices.TaskExecutor.Application.Services.Response;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Services.User
{
    public class UserIdentityServiceClient : IUserIdentityServiceClient
    {
        private readonly IHttpClient _httpClient;
        private readonly string _url;

        public UserIdentityServiceClient(IHttpClient httpClient, HttpClientOptions options)
        {
            _httpClient = httpClient;
            _url = options.Services["identity"];
        }

        public async Task<GetUserIdentityResponse> GetUserIdentity(GetUserIdentityRequest request)
            => ConvertUserDtoToGetUserIdentity(
                await _httpClient.GetAsync<UserDto>($"{_url}/api/identity/{request.Id}"));

        public async Task<IEnumerable<MarketplaceDto>> GetMarketplaces()
            => await _httpClient.GetAsync<IEnumerable<MarketplaceDto>>($"{_url}/api/marketplaces");

        private GetUserIdentityResponse ConvertUserDtoToGetUserIdentity(UserDto userDto)
            => userDto == null
                ? new GetUserIdentityResponse()
                : new GetUserIdentityResponse(
                    userDto.Login, 
                    userDto.Name, 
                    userDto.Login, 
                    userDto.Login);
    }
}