﻿using System;
using System.Linq;
using eBayMicroservices.TaskExecutor.Application.Services;
using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Services
{
    public class IdentityProvider : IIdentityProvider
    {
        public Guid GetIdentity(HttpContext ctx) 
            => Guid.Parse(GetIdentityAsString(ctx));

        public string GetIdentityAsString(HttpContext ctx) 
            => ctx.Request.Headers["Authorization"].FirstOrDefault();
    }
}