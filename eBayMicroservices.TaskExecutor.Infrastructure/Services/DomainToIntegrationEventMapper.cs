﻿using System.Collections.Generic;
using System.Linq;
using Convey.CQRS.Events;
using eBayMicroservices.TaskExecutor.Application.Events;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Events.Abstract;
using eBayMicroservices.TaskExecutor.Core.Events.Concrete;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Services
{
    public class DomainToIntegrationEventMapper : IDomainToIntegrationEventMapper
    {
        public IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events) => events.Select(Map);

        public IEvent Map(IDomainEvent @event) => @event switch
        {
            OperationCreated e => MapOperationCreated(e),
            _ => null
        };
        private static IEvent MapOperationCreated(OperationCreated operationCreated)
        {
            return operationCreated.Operation switch
            {
                RequestOrderReportOperation op => new OrderReportRequested(op.Id.Value),
                _ => null
            };
        }
    }
}