﻿using System;
using Convey;
using Convey.CQRS.Queries;
using Convey.Discovery.Consul;
using Convey.HTTP;
using Convey.LoadBalancing.Fabio;
using Convey.MessageBrokers.CQRS;
using Convey.MessageBrokers.RabbitMQ;
using Convey.Persistence.MongoDB;
using Convey.WebApi;
using eBayMicroservices.TaskExecutor.Application.Events;
using eBayMicroservices.TaskExecutor.Application.Events.External;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Core.Repositories;
using eBayMicroservices.TaskExecutor.Infrastructure.Exceptions;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.CollectionProviding.Concrete;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Concrete;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Repositories;
using eBayMicroservices.TaskExecutor.Infrastructure.Services;
using eBayMicroservices.TaskExecutor.Infrastructure.Services.User;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;

namespace eBayMicroservices.TaskExecutor.Infrastructure
{
    public static class Extensions
    {
        public static IConveyBuilder AddInfrastructure(this IConveyBuilder builder)
        {
            builder
                .AddQueryHandlers()
                .AddInMemoryQueryDispatcher()
                .AddHttpClient()
                .AddErrorHandler<ExceptionToResponseMapper>()
                .AddExceptionToMessageMapper<ExceptionToMessageMapper>()
                .AddRabbitMq()
                .AddMongo()
                .AddConsul()
                .AddFabio()
                //.AddMessageOutbox(o => o.AddMongo())
                ;

            builder.Services.Configure<KestrelServerOptions>((Action<KestrelServerOptions>) (o => o.AllowSynchronousIO = true));
            builder.Services.Configure<IISServerOptions>((Action<IISServerOptions>) (o => o.AllowSynchronousIO = true));
            
            IServiceCollection services = builder.Services;
            services.AddSingleton<IObjectDocumentMapper,ObjectDocumentMapper>();
            services.AddSingleton<IDomainToIntegrationEventMapper,DomainToIntegrationEventMapper>();
            
            services.AddTransient<IOperationRepository,OperationRepository>();
            services.AddTransient<IMongoCollectionProvider,MongoCollectionProvider>();
            
            services.AddTransient<IEventProcessor,EventProcessor>();
            services.AddTransient<IMessageBroker,MessageBroker>();
            services.AddTransient<IAuthenticator,Authenticator>();
            services.AddTransient<IIdentityProvider,IdentityProvider>();
            services.AddTransient<IUserIdentityServiceClient,UserIdentityServiceClient>();
            services.AddTransient<IGuidsFromMessageExpander,GuidsFromMessageExpander>();
            
            if (IsGitlabRunner() || IsNotK8SConfig())
            {
                //services.AddTransient<IEBayRefreshTokenCallExecutor, EBayRefreshTokenCallExecutorMock>(); 
            }
            else
            {
                //services.AddTransient<IEBayRefreshTokenCallExecutor, EBayRefreshTokenCallExecutor>();
            }
            
            //services.TryDecorate(typeof(ICommandHandler<>), typeof(OutboxCommandHandlerDecorator<>));
            //services.TryDecorate(typeof(IEventHandler<>), typeof(OutboxEventHandlerDecorator<>));
            
            builder.Services.Scan(s => s.FromAssemblies(AppDomain.CurrentDomain.GetAssemblies())
                .AddClasses(c => c.AssignableTo(typeof(IDomainEventHandler<>)))
                .AsImplementedInterfaces().WithTransientLifetime());
            
            return builder;
        }
        
        private static bool IsNotK8SConfig()
        {
            return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")?.ToLower() != "kubernetes";
        }
        private static bool IsGitlabRunner()
        {
            return Environment.GetEnvironmentVariable("RUNNER") == "linux";
        }

        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder app)
        {
            app
                .UseErrorHandler()
                .UseConvey()
                //.UsePublicContracts<ContractAttribute>()
                .UseRabbitMq()
                .SubscribeEvent<GenerateReportOrderCompleted>()
                .SubscribeEvent<EBayApiExceptionEvent>()
                ;
            return app;
        }
    }
}