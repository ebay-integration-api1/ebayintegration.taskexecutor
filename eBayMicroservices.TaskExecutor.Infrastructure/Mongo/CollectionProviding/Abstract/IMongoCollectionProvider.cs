﻿using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;
using MongoDB.Driver;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.CollectionProviding.Abstract
{
    public interface IMongoCollectionProvider
    {
        IMongoCollection<T> OperationDocumentCollection<T>() where T : OperationDocumentBase;
    }
}