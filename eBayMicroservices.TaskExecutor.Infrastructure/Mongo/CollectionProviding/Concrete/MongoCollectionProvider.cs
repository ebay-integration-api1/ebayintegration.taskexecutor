﻿using System;
using System.Collections.Generic;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.CollectionProviding.Concrete
{
    public class MongoCollectionProvider : IMongoCollectionProvider
    {
        private readonly IMongoClient _client;
        private readonly string _db;

        public MongoCollectionProvider(IConfiguration configuration)
        {
            string connString = configuration.GetSection("mongo").GetSection("connectionString").Value;
            _db = configuration.GetSection("mongo").GetSection("database").Value;
            _client = new MongoClient(connString);
        }
        
        public IMongoCollection<T> OperationDocumentCollection<T>() where T : OperationDocumentBase
        {
            IMongoCollection<T> mongoCollection =  ReturnMongoCollectionByCollectionName<T>(CollectionNameMapping[typeof(T)]);
            return mongoCollection;
        }

        private IMongoCollection<T> ReturnMongoCollectionByCollectionName<T>(string name)
            where T : OperationDocumentBase
        {
            return _client.GetDatabase(_db).GetCollection<T>(name);
        }

        private static readonly IDictionary<Type, string> CollectionNameMapping = new Dictionary<Type, string>
        {
            {typeof(RequestOrderReportDocument),"RequestOrderReportOperation"},
        };
        
    }
}