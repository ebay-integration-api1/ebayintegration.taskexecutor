﻿namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects
{
    public class RequestOrderReportDocument : OperationDocumentBase
    {
        public long FromDate { get; set; }
        public long ToDate { get; set; }
        public string EBayReportId { get; set; }
        public string Marketplace { get; set; }
    }
}