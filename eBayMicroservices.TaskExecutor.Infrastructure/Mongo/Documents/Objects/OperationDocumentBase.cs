﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects
{
    public abstract class OperationDocumentBase
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public OperationType Type { get; set; }
        public OperationState State { get; set; }
        public Guid? ParentOperationId { get; set; }
        public string ErrorMessage { get; set; }
    }
}