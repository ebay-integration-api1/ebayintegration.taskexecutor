﻿using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Abstract
{
    public interface IObjectDocumentMapper
    {
        RequestOrderReportOperation Convert (RequestOrderReportDocument document);
        RequestOrderReportDocument Convert(RequestOrderReportOperation operation);
    }
}