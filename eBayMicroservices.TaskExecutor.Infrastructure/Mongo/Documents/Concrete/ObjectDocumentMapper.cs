﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Concrete
{
    public class ObjectDocumentMapper : IObjectDocumentMapper
    {
        public RequestOrderReportOperation Convert(RequestOrderReportDocument document)

        {
            return document is null
                ? null
                : new RequestOrderReportOperation(document.Id, document.UserId,new DateTime(document.FromDate),
            new DateTime(document.ToDate), document.EBayReportId, document.ParentOperationId, document.Marketplace,
            document.ErrorMessage);
        }

        public RequestOrderReportDocument Convert(RequestOrderReportOperation operation)
        {
            return operation is null
                ? null
                : new RequestOrderReportDocument()
                {
                    Id = operation.Id.Value,
                    UserId = operation.UserId,
                    FromDate = operation.FromDate.Ticks,
                    ToDate = operation.ToDate.Ticks,
                    EBayReportId = operation.EBayRequestId,
                    Type = operation.Type,
                    ParentOperationId = operation.ParentOperationId,
                    Marketplace = operation.Marketplace,
                    ErrorMessage = operation.ErrorMessage,
                };
        }
    }
}