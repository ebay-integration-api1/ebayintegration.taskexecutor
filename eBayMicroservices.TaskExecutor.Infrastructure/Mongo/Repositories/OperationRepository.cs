﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Repositories;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;
using MongoDB.Driver;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Repositories
{
    public class OperationRepository : IOperationRepository
    {
        private readonly IObjectDocumentMapper _objectToDocumentMapper;
        private readonly IMongoCollectionProvider _mongoCollectionProvider;

        public OperationRepository(IObjectDocumentMapper objectToDocumentMapper,
            IMongoCollectionProvider mongoCollection)
        {
            _objectToDocumentMapper = objectToDocumentMapper;
            _mongoCollectionProvider = mongoCollection;
        }

        public async Task AddAsync<T>(T operation) where T : Operation
        {
            Task taskToExecuteByType = operation switch
            {
                RequestOrderReportOperation requestOrderReportOperation
                    => _mongoCollectionProvider
                        .OperationDocumentCollection<RequestOrderReportDocument>()
                        .InsertOneAsync(_objectToDocumentMapper.Convert(requestOrderReportOperation)),
                _ 
                    => throw new ArgumentOutOfRangeException("Unknown Operation Type")
            };

            await taskToExecuteByType;
        }
        public async Task UpdateRequestOrderReportOperationStateAndEBayId(ICollection<Guid> guids, OperationState state, string eBayReportId)
        {
            FilterDefinition<RequestOrderReportDocument> filter = Builders<RequestOrderReportDocument>.Filter
                .Where(s => guids.Contains(s.Id));

            UpdateDefinition<RequestOrderReportDocument> update =
                Builders<RequestOrderReportDocument>.Update
                    .Set(s => s.State, state)
                    .Set(s => s.EBayReportId, eBayReportId);
            
            await _mongoCollectionProvider.OperationDocumentCollection<RequestOrderReportDocument>()
                .UpdateManyAsync(filter, update);
        }
        public async Task UpdateRequestOrderReportOperationStateAndErrorMessage(ICollection<Guid> guids, OperationState state, string errorMessage)
        {
            FilterDefinition<RequestOrderReportDocument> filter = Builders<RequestOrderReportDocument>.Filter
                .Where(s => guids.Contains(s.Id));

            UpdateDefinition<RequestOrderReportDocument> update =
                Builders<RequestOrderReportDocument>.Update
                    .Set(s => s.State, state)
                    .Set(s => s.ErrorMessage, errorMessage);
            
            await _mongoCollectionProvider.OperationDocumentCollection<RequestOrderReportDocument>()
                .UpdateManyAsync(filter, update);
        }
        public async Task DeleteAsync(Operation operation)
        {
            Task taskToExecute = operation switch
            {
                RequestOrderReportOperation change => DeleteUpdateRequestOrderReportOperation(change),
                _ => throw new ArgumentOutOfRangeException("Unknown Operation Type")
            };

            await taskToExecute;
        }

        private async Task DeleteUpdateRequestOrderReportOperation(RequestOrderReportOperation operation)
        {
            await _mongoCollectionProvider
                .OperationDocumentCollection<RequestOrderReportDocument>()
                .DeleteOneAsync(x => x.Id == operation.Id.Value);
        }
    }
}