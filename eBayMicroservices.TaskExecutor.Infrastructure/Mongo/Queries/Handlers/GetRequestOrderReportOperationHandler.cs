﻿using System;
using System.Threading.Tasks;
using Convey.CQRS.Queries;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Queries;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.CollectionProviding.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;
using MongoDB.Driver;

namespace eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Queries.Handlers
{
    public class GetRequestOrderReportOperationHandler : IQueryHandler<GetRequestOrderReportOperation, RequestOrderReportDto>
    {
        private readonly IMongoCollectionProvider _mongoCollectionProvider;

        public GetRequestOrderReportOperationHandler(IMongoCollectionProvider mongoCollectionProvider)
        {
            _mongoCollectionProvider = mongoCollectionProvider;
        }
        
        public async Task<RequestOrderReportDto> HandleAsync(GetRequestOrderReportOperation query)
        {
            RequestOrderReportDocument requestOrderReportDocument =  await _mongoCollectionProvider
                .OperationDocumentCollection<RequestOrderReportDocument>()
                .Find(x => x.Id == query.OperationId)
                .FirstOrDefaultAsync();

            if (requestOrderReportDocument is null) return null;
            
            return new RequestOrderReportDto
            {
                OperationId = requestOrderReportDocument.Id,
                UserId = requestOrderReportDocument.UserId,
                State = requestOrderReportDocument.State,
                ParentOperationId = requestOrderReportDocument.ParentOperationId,
                FromDate = new DateTime(requestOrderReportDocument.FromDate),
                ToDate = new DateTime(requestOrderReportDocument.ToDate),
                EBayRequestId = requestOrderReportDocument.EBayReportId,
                Marketplace = requestOrderReportDocument.Marketplace,
                ErrorMessage = requestOrderReportDocument.ErrorMessage,
            };
        }
    }
}