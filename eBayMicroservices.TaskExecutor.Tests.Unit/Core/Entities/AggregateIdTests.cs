﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.Unit.Core.Entities
{
    public class AggregateIdTests
    {
        private static AggregateId Act(Guid? id)
            => id.HasValue ? new AggregateId(id.Value) : new AggregateId();

        [Fact]
        public void create_aggregateId_with_known_id_should_be_success()
        {
            Guid id = Guid.NewGuid();

            AggregateId aggregateId = Act(id);
            
            aggregateId.ShouldNotBeNull();
            
            aggregateId.Value.ShouldBe(id);
        }

        [Fact]
        public void create_aggregateId_with_Guid_empty_should_throw_exception()
        {
            Guid id = Guid.Empty;
            
            Exception exception = Record.Exception(() => Act(id));
            
            exception.ShouldNotBeNull();

            exception.ShouldBeOfType<InvalidAggregateIdException>();
            
        }
    }
}