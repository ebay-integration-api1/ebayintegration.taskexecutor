﻿using System;
using System.Linq;
using eBayMicroservices.TaskExecutor.Application.Exceptions;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Events.Abstract;
using eBayMicroservices.TaskExecutor.Core.Events.Concrete;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Concrete;
using Shouldly;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.Unit.Core.Entities
{
    public class CreateRequestOrderReportOperationTests
    {
        private static RequestOrderReportOperation Act(Guid id, Guid userId, DateTime startDate,
            DateTime endDate, string requestId, Guid? parentOperationId, string marketplace, string errorMessage)
            => RequestOrderReportOperation.Create(id, userId, startDate, endDate, requestId, parentOperationId,
                marketplace, errorMessage);
        
        [Fact]
        public void given_all_correct_data_should_be_created()
        {
            AggregateId id = new AggregateId();
            Guid userId = Guid.NewGuid();
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            const string requestId = null;
            Guid? parentOperationId = null;
            const string marketplace = "example";
            const string errorMessage = "error";

            RequestOrderReportOperation operation = Act(id.Value, userId, startDate, endDate, requestId,
                parentOperationId,
                marketplace, errorMessage);

            operation.ShouldNotBeNull();

            operation.Id.Value.ShouldBe(id.Value);
            operation.UserId.ShouldBe(userId);
            operation.FromDate.ShouldBe(startDate);
            operation.ToDate.ShouldBe(endDate);
            operation.EBayRequestId.ShouldBe(requestId);
            operation.ParentOperationId.ShouldBe(parentOperationId);
            operation.Events.Count().ShouldBe(1);

            IDomainEvent @event = operation.Events.FirstOrDefault();

            @event.ShouldBeOfType<OperationCreated>();
        }

        [Fact]
        public void given_empty_user_id_should_throw_exception()
        {
            AggregateId id = new AggregateId();
            Guid userId = Guid.Empty;
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            string requestId = null;
            Guid? parentOperationId = null;
            const string marketplace = "example";
            const string errorMessage = "error";

            Exception exception = Record.Exception(() => Act(id.Value, userId, startDate, endDate, requestId,
                parentOperationId, marketplace, errorMessage));

            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<InvalidUserIdException>();

            InvalidUserIdException typedException = exception as InvalidUserIdException;

            typedException?.Id.ShouldBe(id.Value);
        }
        [Fact]
        public void given_end_date_greater_than_current_date_should_throw_exception()
        {
            AggregateId id = new AggregateId();
            Guid userId = Guid.NewGuid();
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            string requestId = null;
            Guid? parentOperationId = null;
            const string marketplace = "example";
            const string errorMessage = "error";

            DateTime endDateWithAddedMonth = endDate.AddMonths(1);

            Exception exception = Record.Exception(() => Act(id.Value, userId, startDate,
                endDateWithAddedMonth, requestId, parentOperationId, marketplace, errorMessage));

            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<InvalidEndDateException>();

            InvalidEndDateException typedException = exception as InvalidEndDateException;

            typedException?.Id.ShouldBe(id.Value);
        }

        [Fact]
        public void given_start_date_greater_than_end_date_should_throw_exception()
        {
            AggregateId id = new AggregateId();
            Guid userId = Guid.NewGuid();
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            string requestId = null;
            Guid? parentOperationId = null;
            const string marketplace = "example";
            const string errorMessage = "error";

            DateTime startDateWithAddedMonth = startDate.AddMonths(1);

            Exception exception = Record.Exception(() => Act(id.Value, userId, startDateWithAddedMonth,
                endDate, requestId, parentOperationId,marketplace, errorMessage));

            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<InvalidDateRangeException>();

            InvalidDateRangeException typedException = exception as InvalidDateRangeException;

            typedException?.Id.ShouldBe(id.Value);
            
        }
        [Fact]
        public void given_invalid_id_should_throw_exception()
        {
            Guid id = Guid.Empty;
            Guid userId = Guid.NewGuid();
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            string requestId = null;
            Guid? parentOperationId = null;
            const string marketplace = "example";
            const string errorMessage = "error";

            Exception exception = Record.Exception(() => Act(id, userId, startDate, endDate, requestId,
                parentOperationId,
                marketplace, errorMessage));

            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<InvalidAggregateIdException>();
        }
    }
}