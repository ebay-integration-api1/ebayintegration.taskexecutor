﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Concrete;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;
using Shouldly;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.Unit.Infrastructure.Documents.Mapper
{
    public class ConvertRequestOrderReportOperationDocumentTests
    {
        private readonly IObjectDocumentMapper _objectDocumentMapper;

        private RequestOrderReportOperation Act(RequestOrderReportDocument document) =>
            _objectDocumentMapper.Convert(document);
        
        public ConvertRequestOrderReportOperationDocumentTests()
        {
            _objectDocumentMapper = new ObjectDocumentMapper();
        }

        [Fact]
        public void given_null_parameter_should_return_null()
        {
            RequestOrderReportDocument document = null;
            
            RequestOrderReportOperation result = Act(document);
            
            result.ShouldBeNull();
        }
        
        [Fact]
        public void given_valid_parameter_should_be_succeed()
        {
            AggregateId operationId = new AggregateId();
            Guid userId = Guid.NewGuid();
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            string eBayRequestId = "exampleString";
            const OperationState state = OperationState.Accepted;
            const OperationType type = OperationType.RequestOrderReport;
            Guid parentOperationId = Guid.NewGuid();
            
            RequestOrderReportDocument document = new RequestOrderReportDocument
            {
                Id = operationId.Value,
                UserId = userId,
                FromDate = fromDate.Ticks,
                ToDate = toDate.Ticks,
                EBayReportId = eBayRequestId,
                ParentOperationId = parentOperationId,
                State = state,
                Type = type,
            };

            RequestOrderReportOperation result = Act(document);
            
            result.ShouldNotBeNull();
            
            result.Id.ShouldBe(operationId);
            result.UserId.ShouldBe(userId);
            result.FromDate.ShouldBe(fromDate);
            result.ToDate.ShouldBe(toDate);
            result.EBayRequestId.ShouldBe(eBayRequestId);
            result.ParentOperationId.ShouldBe(parentOperationId);
            result.State.ShouldBe(state);
            result.Type.ShouldBe(type);
        }
    }
}