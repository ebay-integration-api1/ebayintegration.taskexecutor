﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Abstract;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Concrete;
using eBayMicroservices.TaskExecutor.Infrastructure.Mongo.Documents.Objects;
using Shouldly;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.Unit.Infrastructure.Documents.Mapper
{
    public class ConvertRequestOrderReportOperationTests
    {
        private readonly IObjectDocumentMapper _objectDocumentMapper;

        private RequestOrderReportDocument Act(RequestOrderReportOperation obj) =>
            _objectDocumentMapper.Convert(obj);
        
        public ConvertRequestOrderReportOperationTests()
        {
            _objectDocumentMapper = new ObjectDocumentMapper();
        }

        [Fact]
        public void given_null_parameter_should_return_null()
        {
            RequestOrderReportOperation obj = null;
            
            RequestOrderReportDocument result = Act(obj);
            
            result.ShouldBeNull();
        }
        
        [Fact]
        public void given_valid_parameter_should_be_succeed()
        {
            Guid operationId = Guid.NewGuid();
            Guid userId = Guid.NewGuid();
            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            string eBayRequestId = "exampleString";
            const OperationState state = OperationState.Accepted;
            const OperationType type = OperationType.RequestOrderReport;
            Guid parentOperationId = Guid.NewGuid();
            const string marketplace = "example";
            const string errorMessage = "error";

            RequestOrderReportOperation obj = new RequestOrderReportOperation(operationId, userId, fromDate, toDate,
                eBayRequestId, parentOperationId,marketplace, errorMessage);

            RequestOrderReportDocument result = Act(obj);
            
            result.ShouldNotBeNull();
            
            result.Id.ShouldBe(operationId);
            result.UserId.ShouldBe(userId);
            result.FromDate.ShouldBe(fromDate.Ticks);
            result.ToDate.ShouldBe(toDate.Ticks);
            result.EBayReportId.ShouldBe(eBayRequestId);
            result.ParentOperationId.ShouldBe(parentOperationId);
            result.State.ShouldBe(state);
            result.Type.ShouldBe(type);
        }
    }
}