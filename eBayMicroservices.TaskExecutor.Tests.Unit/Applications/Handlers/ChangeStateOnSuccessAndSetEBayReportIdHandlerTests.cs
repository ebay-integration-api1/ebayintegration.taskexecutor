﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Application.Commands;
using eBayMicroservices.TaskExecutor.Application.Commands.Handlers;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Repositories;
using NSubstitute;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.Unit.Applications.Handlers
{
    public class ChangeStateOnSuccessAndSetEBayReportIdHandlerTests
    {
        private Task Act(ChangeStateOnSuccessAndSetEBayReportId command) => _handler.HandleAsync(command);

        private readonly ChangeStateOnSuccessAndSetEBayReportIdHandler _handler;
        private readonly IOperationRepository _operationRepository;

        #region Arrange

        public ChangeStateOnSuccessAndSetEBayReportIdHandlerTests()
        {
            _operationRepository = Substitute.For<IOperationRepository>();
            _handler = new ChangeStateOnSuccessAndSetEBayReportIdHandler(_operationRepository);
        }

        #endregion

        [Fact]

        public async Task change_state_on_success_and_set_ebay_report_id_should_succeed()
        {
            IEnumerable<Guid> operationId = new[] { new Guid() };
            OperationState state = OperationState.Completed;
            string eBayRequestId = "exampleString";

            ChangeStateOnSuccessAndSetEBayReportId command = new ChangeStateOnSuccessAndSetEBayReportId(operationId, state, eBayRequestId);

            await Act(command);
        }
    }
}