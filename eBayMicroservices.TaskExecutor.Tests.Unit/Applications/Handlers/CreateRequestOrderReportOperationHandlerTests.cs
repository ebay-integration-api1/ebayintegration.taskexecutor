﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Application.Commands;
using eBayMicroservices.TaskExecutor.Application.Commands.Handlers;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Exceptions;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Application.Services.Request;
using eBayMicroservices.TaskExecutor.Application.Services.Response;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Events.Abstract;
using eBayMicroservices.TaskExecutor.Core.Events.Concrete;
using eBayMicroservices.TaskExecutor.Core.Repositories;
using NSubstitute;
using Shouldly;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.Unit.Applications.Handlers
{
    public class CreateRequestOrderReportOperationHandlerTests
    {
        private Task Act(CreateRequestOrderReportOperation command) => _handler.HandleAsync(command);

        private readonly CreateRequestOrderReportOperationHandler _handler;
        private readonly IOperationRepository _operationRepository;
        private readonly IEventProcessor _eventProcessor;
        private readonly IUserIdentityServiceClient _userIdentityServiceClient;

        #region Arrange

        public CreateRequestOrderReportOperationHandlerTests()
        {
            _operationRepository = Substitute.For<IOperationRepository>();
            _eventProcessor = Substitute.For<IEventProcessor>();
            _userIdentityServiceClient = Substitute.For<IUserIdentityServiceClient>();
            _handler = new CreateRequestOrderReportOperationHandler(_userIdentityServiceClient,
                _eventProcessor, _operationRepository);
        }

        #endregion

        [Fact]
        public async Task create_request_order_report_operation_should_succeed()
        {
            Guid operationId = Guid.NewGuid();
            Guid userId = Guid.NewGuid();

            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            
            const string exampleMwsToken = "exampleMwsToken";
            const string mwsClientId = "mwsClientId";
            const string mwsSecret = "mwsSecret";
            const string mwsEmail = "mwsEmail";
            
            const string marketplace = "EBAY_PL";

            _userIdentityServiceClient.GetUserIdentity(Arg.Any<GetUserIdentityRequest>())
                .Returns(
                    new GetUserIdentityResponse(exampleMwsToken,mwsClientId, mwsSecret, mwsEmail)
                );
            _userIdentityServiceClient.GetMarketplaces()
                .Returns(
                    new List<MarketplaceDto>
                    {
                        new MarketplaceDto
                        {
                            Name = "PL",
                            Code = "EBAY_PL",
                        }
                    }
                );

            CreateRequestOrderReportOperation command =
                new CreateRequestOrderReportOperation(operationId, userId, fromDate, toDate, marketplace);

            await Act(command);

            await _operationRepository.Received()
                .AddAsync(Arg.Is<RequestOrderReportOperation>(x =>
                    x.Id.Value == operationId
                    && x.UserId == userId
                    && x.FromDate == fromDate
                    && x.ToDate == toDate
                    && x.Marketplace == marketplace
                ));

            await _eventProcessor.Received()
                .ProcessAsync(Arg.Is<List<IDomainEvent>>(x =>
                    x.Count == 1
                    && x.Any(e =>
                        e is OperationCreated
                        && ((OperationCreated) e).Operation.Id.Value == operationId
                        && ((OperationCreated) e).Operation.Type == OperationType.RequestOrderReport)
                ));
        }
        
        [Fact]
        public async Task given_invalid_marketplace_should_fail()
        {
            Guid operationId = Guid.NewGuid();
            Guid userId = Guid.NewGuid();

            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            
            const string exampleMwsToken = "exampleMwsToken";
            const string mwsClientId = "mwsClientId";
            const string mwsSecret = "mwsSecret";
            const string mwsEmail = "mwsEmail";
            
            const string marketplace = "marketplace";

            _userIdentityServiceClient.GetUserIdentity(Arg.Any<GetUserIdentityRequest>())
                .Returns(
                    new GetUserIdentityResponse(exampleMwsToken,mwsClientId, mwsSecret, mwsEmail)
                );
            _userIdentityServiceClient.GetMarketplaces()
                .Returns(
                    new List<MarketplaceDto>
                    {
                        new MarketplaceDto
                        {
                            Name = "PL",
                            Code = "EBAY_PL",
                        }
                    }
                );

            CreateRequestOrderReportOperation command =
                new CreateRequestOrderReportOperation(operationId, userId, fromDate, toDate, marketplace);
            
            Exception exception = await Record.ExceptionAsync(() => Act(command));
            
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<InvalidMarketplaceException>();

        }
    }
}