﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Application.Commands;
using eBayMicroservices.TaskExecutor.Application.Commands.Handlers;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Repositories;
using NSubstitute;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.Unit.Applications.Handlers
{
    public class ChangeStateOnFailureAndSetErrorMessageHandlerTests
    {
        private Task Act(ChangeStateOnFailureAndSetErrorMessage command) => _handler.HandleAsync(command);

        private readonly ChangeStateOnFailureAndSetErrorMessageHandler _handler;
        private readonly IOperationRepository _operationRepository;

        #region Arrange

        public ChangeStateOnFailureAndSetErrorMessageHandlerTests()
        {
            _operationRepository = Substitute.For<IOperationRepository>();
            _handler = new ChangeStateOnFailureAndSetErrorMessageHandler(_operationRepository);
        }

        #endregion

        [Fact]

        public async Task change_state_on_success_and_set_ebay_report_id_should_succeed()
        {
            IEnumerable<Guid> operationId = new[] { new Guid() };
            OperationState state = OperationState.Error;
            string errorMessage = "error";

            ChangeStateOnFailureAndSetErrorMessage command = new ChangeStateOnFailureAndSetErrorMessage(operationId, state, errorMessage);

            await Act(command);
        }
    }
}