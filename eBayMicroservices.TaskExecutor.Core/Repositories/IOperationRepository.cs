﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Core.Repositories
{
    public interface IOperationRepository
    {
        Task AddAsync<T>(T operation) where T : Operation;
        Task UpdateRequestOrderReportOperationStateAndEBayId(ICollection<Guid> guids,
            OperationState state, string eBayId);
        Task UpdateRequestOrderReportOperationStateAndErrorMessage(ICollection<Guid> guids,
            OperationState state, string errorMessage);
    }
}