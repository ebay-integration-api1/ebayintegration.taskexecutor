﻿using System;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Abstract;

namespace eBayMicroservices.TaskExecutor.Core.Exceptions.Concrete
{
    public class InvalidAggregateIdException : DomainException
    {
        public override string Code { get; } = "invalid_aggregate_id";
        public Guid Id { get; }

        public InvalidAggregateIdException(Guid id) : base($"Invalid aggregate id: {id}")
            => Id = id;
    }
}