﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Abstract;

namespace eBayMicroservices.TaskExecutor.Core.Exceptions.Concrete
{
    public class InvalidEndDateException : DomainException
    {
        public Guid Id { get; }
        public DateTime EndDate { get; }

        public InvalidEndDateException(Guid id, OperationType operationType, DateTime endDate)
            : base($"invalid userId. Operation id {id}, operationType: {operationType}, endDate: {endDate}")
        {
            Id = id;
            EndDate = endDate;
        }

        public override string Code => "invalid_endDate";
    }
}