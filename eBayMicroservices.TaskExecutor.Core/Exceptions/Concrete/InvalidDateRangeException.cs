﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Abstract;

namespace eBayMicroservices.TaskExecutor.Core.Exceptions.Concrete
{
    public class InvalidDateRangeException : DomainException
    {
        public Guid Id { get; }
        public DateTime StartDate { get; }
        public DateTime EndDate { get; }

        public InvalidDateRangeException(Guid id, OperationType operationType, DateTime startDate, DateTime endDate)
            : base($"invalid date range. Operation id {id}, operationType: {operationType}, startDate: {startDate}, endDate: {endDate}")
        {
            Id = id;
            StartDate = startDate;
            EndDate = endDate;
        }

        public override string Code => "invalid_dateRange";
    }
}