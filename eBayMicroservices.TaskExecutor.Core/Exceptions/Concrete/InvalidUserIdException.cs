﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Abstract;

namespace eBayMicroservices.TaskExecutor.Core.Exceptions.Concrete
{
    public class InvalidUserIdException : DomainException
    {
        public Guid Id { get; }
        public Guid UserId { get; }

        public InvalidUserIdException(Guid id, OperationType operationType, Guid userId)
            : base($"invalid userId. Operation id {id}, operationType: {operationType}, userId: {userId}")
        {
            Id = id;
            UserId = userId;
        }

        public override string Code => "invalid_userId";
    }
}