﻿namespace eBayMicroservices.TaskExecutor.Core.Entities
{
    public enum OperationState
    {
        Accepted = 0,
        InProgress = 1,
        Completed = 2,
        Error = -1
    }
}