﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace eBayMicroservices.TaskExecutor.Core.Entities
{
    public abstract class Operation : AggregateRoot
    {
        public Guid UserId { get; }
        public abstract OperationType Type { get; }
        public OperationState State { get; private set; }
        public Guid? ParentOperationId { get; }
        public string ErrorMessage { get; }

        protected Operation(Guid id, Guid userId, Guid? parentOperationId = null, string errorMessage = null)
        {
            Id = new AggregateId(id);
            UserId = userId;
            ParentOperationId = parentOperationId;
            ErrorMessage = errorMessage;
        }

        protected static bool IsUserIdValid(Guid userId)
            => userId != Guid.Empty;

        protected static bool IsEndDateValid(DateTime endDate)
            => endDate <= DateTime.UtcNow;
        
        protected static bool IsDateRangeValid(DateTime startDate, DateTime endDate)
            => endDate >= startDate;
    }
}