﻿using System;
using eBayMicroservices.TaskExecutor.Core.Events.Concrete;
using eBayMicroservices.TaskExecutor.Core.Exceptions.Concrete;

namespace eBayMicroservices.TaskExecutor.Core.Entities
{
    public class RequestOrderReportOperation : Operation
    {
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public string EBayRequestId { get; }
        public string Marketplace { get; }

        public RequestOrderReportOperation(Guid id, Guid userId, DateTime fromDate, DateTime toDate, string eBayRequestId,
            Guid? parentOperationId, string marketplace, string errorMessage) : 
            base(id, userId, parentOperationId, errorMessage)
        {
            FromDate = fromDate;
            ToDate = toDate;
            EBayRequestId = eBayRequestId;
            Marketplace = marketplace;
        }

        public static RequestOrderReportOperation Create(Guid id, Guid userId, DateTime startDate, DateTime endDate, string requestId,
            Guid? parentOperationId, string marketplace, string errorMessage)
        {
            if (!IsDateRangeValid(startDate, endDate)) throw new InvalidDateRangeException(id, CurrentType, startDate, endDate);

            if (!IsEndDateValid(endDate)) throw new InvalidEndDateException(id, CurrentType, endDate);
            
            if(!IsUserIdValid(userId)) throw new InvalidUserIdException(id, CurrentType, userId);
            
            RequestOrderReportOperation orderReportOperation = new RequestOrderReportOperation(id, userId,
                startDate, endDate, requestId, parentOperationId, marketplace, errorMessage);
            
            orderReportOperation.AddEvent(new OperationCreated(orderReportOperation));
            
            return orderReportOperation;
        }

        public override OperationType Type => CurrentType;
        private static OperationType CurrentType => OperationType.RequestOrderReport;
    }
}