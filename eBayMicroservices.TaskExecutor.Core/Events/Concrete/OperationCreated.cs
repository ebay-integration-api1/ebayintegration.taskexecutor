﻿using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Events.Abstract;

namespace eBayMicroservices.TaskExecutor.Core.Events.Concrete
{
    public class OperationCreated : IDomainEvent
    {
        public Operation Operation { get; }

        public OperationCreated(Operation operation)
        {
            Operation = operation;
        }
    }
}