﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Api;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Tests.Shared.Factories;

using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class GetRequestOrderReportOperationDataResult : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private Task<HttpResponseMessage> Act(Guid id)
        {
            Guid authorizationId = Guid.NewGuid();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorizationId.ToString());
            
            string url = @"api/operation/requestOrderReport/" + $"{id.ToString()}";
            return _httpClient.GetAsync(url);
        }

        [Fact]
        public async Task get_with_invalid_request_id_should_return_404()
        {
            Guid emptyId = Guid.Empty;
            HttpResponseMessage response = await Act(emptyId);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.NotFound);
        }
        
        [Fact]
        public async Task get_not_existing_report_should_return_404()
        {
            Guid notExistingId = Guid.NewGuid();
            HttpResponseMessage response = await Act(notExistingId);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.NotFound);
        }
        
        [Fact]
        public async Task get_existing_report_should_succeed()
        {
            Guid existingId = new Guid("c28e4573-7468-431d-9678-d9adf6232a85");
            HttpResponseMessage response = await Act(existingId);
            
            response.ShouldNotBeNull();
        
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            
            response.Content.ShouldNotBeNull();

            RequestOrderReportDto result = JsonConvert.DeserializeObject<RequestOrderReportDto>(await response.Content.ReadAsStringAsync());
            
            result.ShouldNotBeNull();
            result.OperationId.ShouldBe(existingId);
        }

        private readonly HttpClient _httpClient;
        public GetRequestOrderReportOperationDataResult(EBayMicroserviceAppFactory<Program> factory)
        {
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
        }
        
        public void Dispose()
        {
            //_mongoDbFixture.Dispose();
        }
    }
}