﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Api;
using eBayMicroservices.TaskExecutor.Api.Models;
using eBayMicroservices.TaskExecutor.Tests.Shared.Factories;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace eBayMicroservices.TaskExecutor.Tests.EndToEnd.Sync
{
    [Collection("E2E")]
    public class RequestOrderReportOperationResult : IDisposable,IClassFixture<EBayMicroserviceAppFactory<Program>>
    {
        private readonly HttpClient _httpClient;

        #region  Arrange

        public RequestOrderReportOperationResult(EBayMicroserviceAppFactory<Program> factory)
        {
            factory.Server.AllowSynchronousIO = true;
            _httpClient = factory.CreateClient();
        }
        
        #endregion
        
        private Task<HttpResponseMessage> Act(CreateRequestOrderReportOperationModel request)
        {
            Guid authorizationId = Guid.NewGuid();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorizationId.ToString());
            
            string url = @"api/operation/requestOrderReport";
            string payload = JsonConvert.SerializeObject(request);
            HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");

            return _httpClient.PostAsync(url, content);
        }

        [Fact]
        public async Task create_request_order_report_operation_should_succeeded()
        {
            CreateRequestOrderReportOperationModel request = new CreateRequestOrderReportOperationModel()
            {
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                Marketplace = "EBAY_PL",
            };
            HttpResponseMessage response = await Act(request);
            
            response.ShouldNotBeNull();
            
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
        }

        public void Dispose()
        {
            //_mongoDbFixture.Dispose();
        }
    }
}