﻿using System;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Infrastructure.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;

namespace eBayMicroservices.TaskExecutor.Tests.Shared.Factories
{
    public class EBayMicroserviceAppFactory<TEntryPoint> : WebApplicationFactory<TEntryPoint> where TEntryPoint : class
    {
        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            
            var p = Environment.GetEnvironmentVariable("RUNNER");
            Console.WriteLine($"RUNNER variable is : {p}");
            if(p == "linux") return base.CreateWebHostBuilder().UseEnvironment("tests_in_runner");
            return base.CreateWebHostBuilder().UseEnvironment("tests")
                .ConfigureServices(x =>
                    x.AddTransient<IUserIdentityServiceClient, UserIdentityServiceClientMock>()
                    );
        }
    }
}