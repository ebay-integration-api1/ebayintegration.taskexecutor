﻿using System;
using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Application.DTOs
{
    public abstract class OperationDtoBase
    {
        /// <summary>
        /// Internal id of the Operation
        /// </summary>
        public Guid OperationId { get; set; }
        /// <summary>
        /// Id of the user requesting operation
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// A State of the Operation
        /// </summary>
        public OperationState State { get; set; }
        /// <summary>
        /// Id of Parent Operation
        /// </summary>
        public Guid? ParentOperationId { get; set; }
    }
}