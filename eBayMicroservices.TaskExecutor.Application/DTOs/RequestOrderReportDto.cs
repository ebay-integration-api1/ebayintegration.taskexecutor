﻿using System;

namespace eBayMicroservices.TaskExecutor.Application.DTOs
{
    public class RequestOrderReportDto : OperationDtoBase
    {
        /// <summary>
        /// Date from which the report will be requested
        /// </summary>
        public DateTime FromDate { get; set; }
        /// <summary>
        /// Date up to which the report will be requested.
        /// Cannot be greater than current date
        /// </summary>
        public DateTime ToDate { get; set; }
        /// <summary>
        /// Request id provided by EBay, used for further calls to EBay
        /// </summary>
        public string EBayRequestId { get; set; }
        /// <summary>
        /// Marketplace Code used to determine EBay marketplace which will be used to generate the report
        /// </summary>
        public string Marketplace { get; set; }
        /// <summary>
        /// Error message used to determine whether there was an error and what has happened
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}