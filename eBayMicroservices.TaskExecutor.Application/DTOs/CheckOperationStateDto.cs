﻿using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Application.DTOs
{
    public class CheckOperationStateDto
    {
        /// <summary>
        /// Request id provided by EBay, used for further calls to EBay
        /// </summary>
        public string EBayRequestId { get; set; }
        /// <summary>
        /// A State of the Operation
        /// </summary>
        public string OperationState { get; set; }
    }
}