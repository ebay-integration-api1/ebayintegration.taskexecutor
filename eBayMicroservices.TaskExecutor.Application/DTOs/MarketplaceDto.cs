﻿namespace eBayMicroservices.TaskExecutor.Application.DTOs
{
    public class MarketplaceDto
    {
        /// <summary>
        /// Marketplace Code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Marketplace Name
        /// </summary>
        public string Name { get; set; }
    }
}