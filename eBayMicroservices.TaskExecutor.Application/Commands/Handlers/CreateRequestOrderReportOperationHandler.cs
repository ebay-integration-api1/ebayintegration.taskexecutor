﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Convey.CQRS.Commands;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Exceptions;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Core.Entities;
using eBayMicroservices.TaskExecutor.Core.Repositories;

namespace eBayMicroservices.TaskExecutor.Application.Commands.Handlers
{
    public class CreateRequestOrderReportOperationHandler : ICommandHandler<CreateRequestOrderReportOperation>
    {
        private readonly IUserIdentityServiceClient _userIdentityServiceClient;
        private readonly IEventProcessor _eventProcessor;
        private readonly IOperationRepository _operationRepository;

        public CreateRequestOrderReportOperationHandler(IUserIdentityServiceClient userIdentityServiceClient,
            IEventProcessor eventProcessor, IOperationRepository operationRepository)
        {
            _userIdentityServiceClient = userIdentityServiceClient;
            _eventProcessor = eventProcessor;
            _operationRepository = operationRepository;
        }
        
        public async Task HandleAsync(CreateRequestOrderReportOperation command)
        {

            if (string.IsNullOrWhiteSpace(command.Marketplace) || await NotExistsOnAvailableMarketplacesList(command))
            {
                throw new InvalidMarketplaceException(command.Marketplace);
            }
            
            RequestOrderReportOperation orderReportOperation =
                RequestOrderReportOperation.Create(command.OperationId, command.UserId, command.FromDate,
                    command.ToDate, command.EBayRequestId, command.ParentOperationId, command.Marketplace, command.ErrorMessage);
            await _operationRepository.AddAsync(orderReportOperation);
            await _eventProcessor.ProcessAsync(orderReportOperation.Events);
        }

        private async Task<bool> NotExistsOnAvailableMarketplacesList(CreateRequestOrderReportOperation command)
        {
            IEnumerable<MarketplaceDto> marketplaces = await _userIdentityServiceClient.GetMarketplaces();
            return !marketplaces.Any(x => x.Code.Equals(command.Marketplace));
        }
    }
}