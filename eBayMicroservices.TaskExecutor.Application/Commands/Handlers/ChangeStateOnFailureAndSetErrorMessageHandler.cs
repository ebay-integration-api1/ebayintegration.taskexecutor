﻿using System.Collections.Immutable;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Core.Repositories;

namespace eBayMicroservices.TaskExecutor.Application.Commands.Handlers
{
    public class ChangeStateOnFailureAndSetErrorMessageHandler
    {
        private readonly IOperationRepository _operationRepository;

        public ChangeStateOnFailureAndSetErrorMessageHandler(IOperationRepository operationRepository)
        {
            _operationRepository = operationRepository;
        }

        public async Task HandleAsync(ChangeStateOnFailureAndSetErrorMessage command)
            => await _operationRepository.UpdateRequestOrderReportOperationStateAndErrorMessage(
                command.OperationId.ToImmutableList(),
                command.State,
                command.ErrorMessage
            );
    }
}