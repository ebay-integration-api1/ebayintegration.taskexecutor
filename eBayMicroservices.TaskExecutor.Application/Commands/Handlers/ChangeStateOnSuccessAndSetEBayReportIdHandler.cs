﻿using System.Collections.Immutable;
using System.Threading.Tasks;
using Convey.CQRS.Commands;
using eBayMicroservices.TaskExecutor.Core.Repositories;

namespace eBayMicroservices.TaskExecutor.Application.Commands.Handlers
{
    public class ChangeStateOnSuccessAndSetEBayReportIdHandler : ICommandHandler<ChangeStateOnSuccessAndSetEBayReportId>
    {
        private readonly IOperationRepository _operationRepository;

        public ChangeStateOnSuccessAndSetEBayReportIdHandler(IOperationRepository operationRepository)
        {
            _operationRepository = operationRepository;
        }

        public async Task HandleAsync(ChangeStateOnSuccessAndSetEBayReportId command)
            => await _operationRepository.UpdateRequestOrderReportOperationStateAndEBayId(
                command.OperationId.ToImmutableList(),
                command.State,
                command.EBayRequestId
            );

    }
}