﻿using System;
using System.Collections.Generic;
using Convey.CQRS.Commands;
using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Application.Commands
{
    public class ChangeStateOnFailureAndSetErrorMessage : ICommand
    {
        public IEnumerable<Guid> OperationId { get; }
        public OperationState State { get; }
        public string ErrorMessage { get; }


        public ChangeStateOnFailureAndSetErrorMessage(IEnumerable<Guid> operationId, OperationState state,
            string errorMessage)
        {
            OperationId = operationId;
            State = state;
            ErrorMessage = errorMessage;
        }
    }
}