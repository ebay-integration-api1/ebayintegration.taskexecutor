﻿using System;
using Convey.CQRS.Commands;

namespace eBayMicroservices.TaskExecutor.Application.Commands
{
    public class CreateRequestOrderReportOperation : ICommand
    {
        public Guid OperationId { get; }
        public Guid UserId { get; }
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public string EBayRequestId { get; }
        public Guid? ParentOperationId { get; }
        public string Marketplace { get; }
        public string ErrorMessage { get; }

        public CreateRequestOrderReportOperation(Guid operationId, Guid userId, DateTime fromDate, DateTime toDate, string marketplace,
            string eBayRequestId = null, Guid? parentOperationId = null, string errorMessage = null)
        {
            OperationId = operationId;
            UserId = userId;
            FromDate = fromDate;
            ToDate = toDate;
            EBayRequestId = eBayRequestId;
            ParentOperationId = parentOperationId;
            Marketplace = marketplace;
            ErrorMessage = errorMessage;
        }
    }
}