﻿using System;
using System.Collections.Generic;
using Convey.CQRS.Commands;
using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Application.Commands
{
    public class ChangeStateOnSuccessAndSetEBayReportId : ICommand
    {
        public IEnumerable<Guid> OperationId { get; }
        public OperationState State { get; }
        public string EBayRequestId { get; }

        public ChangeStateOnSuccessAndSetEBayReportId(IEnumerable<Guid> operationId, OperationState state,
            string eBayRequestId)
        {
            OperationId = operationId;
            State = state;
            EBayRequestId = eBayRequestId;
        }

    }
}