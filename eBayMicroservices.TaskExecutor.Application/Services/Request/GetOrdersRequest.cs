﻿using System;

namespace eBayMicroservices.TaskExecutor.Application.Services.Request
{
    public class GetOrdersRequest
    {
        public Guid UserId { get; }
        public DateTime Date { get; }

        public GetOrdersRequest(Guid userId, DateTime date)
        {
            UserId = userId;
            Date = date;
        }
    }
}