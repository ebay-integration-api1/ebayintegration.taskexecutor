﻿using System;

namespace eBayMicroservices.TaskExecutor.Application.Services.Request
{
    public class GetUserIdentityRequest
    {
        public Guid Id { get; }

        public GetUserIdentityRequest(Guid id)
        {
            Id = id;
        }
    }
}