﻿using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Core.Events.Abstract;

namespace eBayMicroservices.TaskExecutor.Application.Services
{
    public interface IEventProcessor
    {
        Task ProcessAsync(IEnumerable<IDomainEvent> events);
    }
}