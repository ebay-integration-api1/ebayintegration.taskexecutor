﻿using System;
using System.Collections.Generic;

namespace eBayMicroservices.TaskExecutor.Application.Services
{
    public interface IGuidsFromMessageExpander
    {
        IEnumerable<Guid> ExpandGuidsFromMessage(string messageContent);
    }
}