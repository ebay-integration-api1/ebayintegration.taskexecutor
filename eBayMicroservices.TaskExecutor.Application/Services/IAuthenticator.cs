﻿using Microsoft.AspNetCore.Http;

namespace eBayMicroservices.TaskExecutor.Application.Services
{
    public interface IAuthenticator
    {
        public void Authenticate(HttpContext ctx);
    }
}