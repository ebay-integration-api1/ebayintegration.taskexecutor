﻿using System.Collections.Generic;
using Convey.CQRS.Events;
using eBayMicroservices.TaskExecutor.Core.Events.Abstract;

namespace eBayMicroservices.TaskExecutor.Application.Services
{
    public interface IDomainToIntegrationEventMapper
    {
        IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events);
        IEvent Map(IDomainEvent @event);
        
    }
}