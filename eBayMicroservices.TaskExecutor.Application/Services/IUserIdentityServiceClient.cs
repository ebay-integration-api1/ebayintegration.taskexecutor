﻿using System.Collections.Generic;
using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Services.Request;
using eBayMicroservices.TaskExecutor.Application.Services.Response;

namespace eBayMicroservices.TaskExecutor.Application.Services
{
    public interface IUserIdentityServiceClient
    {
        Task<GetUserIdentityResponse> GetUserIdentity(GetUserIdentityRequest request);
        Task<IEnumerable<MarketplaceDto>> GetMarketplaces();
    }
}