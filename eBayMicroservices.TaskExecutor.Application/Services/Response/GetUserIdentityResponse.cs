﻿namespace eBayMicroservices.TaskExecutor.Application.Services.Response
{
    public class GetUserIdentityResponse
    {
        public GetUserIdentityResponse(string mwsTokenId, string mwsClientId, string mwsSecret, string email)
        {
            MwsTokenId = mwsTokenId;
            MwsClientId = mwsClientId;
            MwsSecret = mwsSecret;
            Email = email;
            UserFound = true;
        }

        public GetUserIdentityResponse()
        {
            UserFound = false;
        }

        public bool UserFound { get; }
        public string MwsTokenId { get; }
        public string MwsClientId { get; }
        public string MwsSecret { get; }
        public string Email { get; }
    }
}