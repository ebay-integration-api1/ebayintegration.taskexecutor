﻿namespace eBayMicroservices.TaskExecutor.Application.Exceptions
{
    public class InvalidMarketplaceException: AppException
    {
        public InvalidMarketplaceException(string marketplace) : base($"invalid marketplace: {marketplace}")
        {
        }

        public override string Code => "given_marketplace_is_invalid";
    }
}