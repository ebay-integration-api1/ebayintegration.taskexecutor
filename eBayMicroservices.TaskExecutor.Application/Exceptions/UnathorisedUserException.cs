﻿using System;

namespace eBayMicroservices.TaskExecutor.Application.Exceptions
{
    public class UnauthorisedUserException : Exception
    {
        public string Code => "unauthorised_user";

        public UnauthorisedUserException() : base("Unauthorised user in secured method")
        {
            
        }
    }
}