﻿using System;
using Convey.CQRS.Queries;
using eBayMicroservices.TaskExecutor.Application.DTOs;

namespace eBayMicroservices.TaskExecutor.Application.Queries
{
    public class GetRequestOrderReportOperation : IQuery<RequestOrderReportDto>
    {
        public Guid OperationId { get; }

        public GetRequestOrderReportOperation(Guid operationId)
        {
            OperationId = operationId;
        }
    }
}