﻿using System.Threading.Tasks;
using eBayMicroservices.TaskExecutor.Core.Events.Abstract;

namespace eBayMicroservices.TaskExecutor.Application.Events
{
    public interface IDomainEventHandler<in T> where T : class, IDomainEvent
    {
        Task HandleAsync(T @event);
    }
}