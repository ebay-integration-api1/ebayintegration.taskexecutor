﻿using System;
using Convey.CQRS.Events;

namespace eBayMicroservices.TaskExecutor.Application.Events
{
    public abstract class OperationAddedBase : IEvent
    {
        protected OperationAddedBase(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; }
    }
}