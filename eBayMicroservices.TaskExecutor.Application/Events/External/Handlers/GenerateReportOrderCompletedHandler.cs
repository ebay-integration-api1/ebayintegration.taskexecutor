﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.CQRS.Commands;
using Convey.CQRS.Events;
using eBayMicroservices.TaskExecutor.Application.Commands;
using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Application.Events.External.Handlers
{
    public class GenerateReportOrderCompletedHandler : IEventHandler<GenerateReportOrderCompleted>
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public GenerateReportOrderCompletedHandler(ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
        }

        public async Task HandleAsync(GenerateReportOrderCompleted @event)
        {
            IEnumerable<Guid> id = new[] {@event.Id};
            ChangeStateOnSuccessAndSetEBayReportId command = new ChangeStateOnSuccessAndSetEBayReportId(
                id, OperationState.Completed, @event.EBayRequestId);
            
            await _commandDispatcher.SendAsync(command);
        }
    }
}