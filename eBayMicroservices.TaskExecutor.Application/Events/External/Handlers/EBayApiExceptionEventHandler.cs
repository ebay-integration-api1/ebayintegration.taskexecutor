﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Convey.CQRS.Commands;
using Convey.CQRS.Events;
using eBayMicroservices.TaskExecutor.Application.Commands;
using eBayMicroservices.TaskExecutor.Core.Entities;

namespace eBayMicroservices.TaskExecutor.Application.Events.External.Handlers
{
    public class EBayApiExceptionEventHandler : IEventHandler<EBayApiExceptionEvent>
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public EBayApiExceptionEventHandler(ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
        }
        
        public async Task HandleAsync(EBayApiExceptionEvent @event)
        {
            IEnumerable<Guid> id = new[] {@event.OperationId};
            ChangeStateOnFailureAndSetErrorMessage command = new ChangeStateOnFailureAndSetErrorMessage(id,
                OperationState.Error, @event.Reason);
            
            await _commandDispatcher.SendAsync(command);
        }
    }
}