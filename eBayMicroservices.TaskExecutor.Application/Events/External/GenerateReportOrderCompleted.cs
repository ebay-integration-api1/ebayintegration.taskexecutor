﻿using System;
using Convey.CQRS.Events;
using Convey.MessageBrokers;

namespace eBayMicroservices.TaskExecutor.Application.Events.External
{
    [Message("feed")]
    public class GenerateReportOrderCompleted : IEvent
    {
        public Guid Id { get; set; }
        public string EBayRequestId { get; set; }
    }
}