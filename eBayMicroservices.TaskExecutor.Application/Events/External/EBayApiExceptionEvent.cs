﻿using System;
using Convey.CQRS.Events;
using Convey.MessageBrokers;

namespace eBayMicroservices.TaskExecutor.Application.Events.External
{
    [Message("feed")]
    public class EBayApiExceptionEvent : IEvent
    {
        public Guid OperationId { get; }
        public string Context { get; }
        public int? StatusCode { get; }
        public string Reason { get; }
        public string Code { get; }
    }
}