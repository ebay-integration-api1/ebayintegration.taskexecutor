﻿using System;

namespace eBayMicroservices.TaskExecutor.Application.Events
{
    public class OrderReportRequested : OperationAddedBase
    {
        public OrderReportRequested(Guid id) : base(id)
        {

        }
    }
}