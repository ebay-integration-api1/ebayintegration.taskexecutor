﻿using System;
using System.Threading.Tasks;
using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using eBayMicroservices.TaskExecutor.Api.Models;
using eBayMicroservices.TaskExecutor.Application.Attributes;
using eBayMicroservices.TaskExecutor.Application.Commands;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Queries;
using eBayMicroservices.TaskExecutor.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.TaskExecutor.Api.Controllers
{
    [ApiController]
    [Route("api/operation")]
    public class RequestOrderReportOperationController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly IIdentityProvider _identityProvider;

        public RequestOrderReportOperationController(ICommandDispatcher commandDispatcher,
            IQueryDispatcher queryDispatcher, IIdentityProvider identityProvider)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
            _identityProvider = identityProvider;
        }

        /// <summary>
        /// Request order report
        /// </summary>
        /// <remarks>
        /// Used to request generation of order report using given time interval.
        /// Dates are considered inclusive.
        /// </remarks>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("requestOrderReport")]
        [OnlyLoggedInUser]
        [ProducesResponseType(typeof(string),200)]
        [ProducesResponseType(typeof(string),400)]
        public async Task<ActionResult> CreateRequestOrderReportOperation(
            CreateRequestOrderReportOperationModel request)
        {
            Guid operationId = Guid.NewGuid();
            
            Guid userId = _identityProvider.GetIdentity(HttpContext);

            CreateRequestOrderReportOperation command = new CreateRequestOrderReportOperation(operationId, userId, request.FromDate,
                request.ToDate, request.Marketplace);
            await _commandDispatcher.SendAsync(command);
            return Ok(operationId);
        }
        
        /// <summary>
        /// Request operation data [INTERNAL]
        /// </summary>
        /// <remarks>
        /// Used to obtain operation data.
        /// Provides information stored in database about operation with given id.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
#if !DEBUG
        [ApiExplorerSettings(IgnoreApi = true)]
#endif
        [HttpGet("requestOrderReport/{id:Guid}")]
        [ProducesResponseType(typeof(RequestOrderReportDto),200)]
        [ProducesResponseType(typeof(string),404)]
        public async Task<ActionResult<RequestOrderReportDto>> GetRequestOrderReportData(Guid id)
        {
            GetRequestOrderReportOperation query = new GetRequestOrderReportOperation(id);
            
            RequestOrderReportDto dto = await _queryDispatcher.QueryAsync(query);

            if (dto is null) return NotFound();
            
            return dto;
        }
    }
}