﻿using System;
using System.Threading.Tasks;
using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using eBayMicroservices.TaskExecutor.Application.Attributes;
using eBayMicroservices.TaskExecutor.Application.DTOs;
using eBayMicroservices.TaskExecutor.Application.Queries;
using eBayMicroservices.TaskExecutor.Application.Services;
using eBayMicroservices.TaskExecutor.Core.Entities;
using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.TaskExecutor.Api.Controllers
{
    [ApiController]
    [Route("api/operation")]
    public class CheckOperationStateController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly IIdentityProvider _identityProvider;

        public CheckOperationStateController(ICommandDispatcher commandDispatcher,
            IQueryDispatcher queryDispatcher, IIdentityProvider identityProvider)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
            _identityProvider = identityProvider;
        }

        /// <summary>
        /// Check operation state
        /// </summary>
        /// <remarks>
        /// Used to check the state of a given operation.
        /// Additionally, if state is "Completed", EBay request id will also be provided.
        /// If operation is not completed, the request id will be null.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        
        [HttpGet("checkOperationState/{id:Guid}")]
        [OnlyLoggedInUser]
        [ProducesResponseType(typeof(CheckOperationStateDto),200)]
        [ProducesResponseType(typeof(string),404)]
        public async Task<ActionResult<OperationState>> GetRequestOrderReportOperationState(Guid id)
        {
            GetRequestOrderReportOperation query = new GetRequestOrderReportOperation(id);
            
            RequestOrderReportDto dto = await _queryDispatcher.QueryAsync(query);

            if (dto is null) return NotFound();
            
            CheckOperationStateDto operationState = new CheckOperationStateDto()
            {
                EBayRequestId = dto.EBayRequestId,
                OperationState = dto.State.ToString()
            };
            
            return Ok(operationState);
        }
    }
}