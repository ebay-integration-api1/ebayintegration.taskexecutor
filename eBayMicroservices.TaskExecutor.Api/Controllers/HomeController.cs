﻿using Microsoft.AspNetCore.Mvc;

namespace eBayMicroservices.TaskExecutor.Api.Controllers
{
    [ApiController]
    [Route("api")]
    public class HomeController : ControllerBase
    {
#if !DEBUG
        [ApiExplorerSettings(IgnoreApi = true)]
#endif
        /// <summary>
        /// [INTERNAL]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Hello()
        {
            return Ok("Hello from eBay Task Executor Service");
        }
        
#if !DEBUG
        [ApiExplorerSettings(IgnoreApi = true)]
#endif
        /// <summary>
        /// [INTERNAL]
        /// </summary>
        /// <returns></returns>
        [HttpGet("/ping")]
        public string HealthCheck() => "Ping OK";
    }
}    