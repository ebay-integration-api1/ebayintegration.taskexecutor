using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Convey;
using Convey.Logging;
using eBayMicroservices.TaskExecutor.Api.AttributesDefinitions;
using eBayMicroservices.TaskExecutor.Application;
using eBayMicroservices.TaskExecutor.Infrastructure;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Open.Serialization.Json;
using Open.Serialization.Json.Newtonsoft;

namespace eBayMicroservices.TaskExecutor.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await CreateWebHostBuilder(args)
                .Build()
                .RunAsync();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(
                    services =>
                    {
                        services.AddControllers().AddNewtonsoftJson();
                        services
                            .AddSingleton<IJsonSerializer>(new JsonSerializerFactory().GetSerializer())
                            .AddSwaggerGen(c =>
                            {
                                c.AddSecurityDefinition("Authorization",new OpenApiSecurityScheme
                                {
                                    Description = "Set inside token returned from Sign-in method from /api/identity/sign-in",
                                    In = ParameterLocation.Header,
                                    Name = "JWT",
                                    Type = SecuritySchemeType.ApiKey,
                                    Scheme = "Authorization"
                                });
                                c.SwaggerDoc("v1", new OpenApiInfo
                                {
                                    Version = "v1",
                                    Title = "eBay Task Executor Microservice",
                                    Description = "Microservice which provides methods to request reports from eBay"
                                });
                                
                                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                                var xmlFile2 = $"eBayMicroservices.TaskExecutor.Application.xml";
                                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                                var xmlPath2 = Path.Combine(AppContext.BaseDirectory, xmlFile2);
                                c.OperationFilter<BasicAuthOperationsFilter>();
                                c.IncludeXmlComments(xmlPath);
                                c.IncludeXmlComments(xmlPath2);
                            })
                            .AddConvey()
                            .AddInfrastructure()
                            .AddApplication();

                        services.BuildServiceProvider();
                    }
                ).Configure(app =>
                    app.UseInfrastructure()
                        .UseSwagger(c =>
                        {
                            c.RouteTemplate = "api/task-executor/swagger/{documentname}/swagger.json";
                        })
                        .UseSwaggerUI(c =>
                        {
                            c.SwaggerEndpoint("/api/task-executor/swagger/v1/swagger.json", "My API V1");
                            c.RoutePrefix = "api/task-executor/swagger";
                        })
                        .UseRouting()
                        .UseEndpoints(e => e.MapControllers())
                )
                .UseLogging();
    }
}