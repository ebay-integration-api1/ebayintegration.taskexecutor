﻿using System;

namespace eBayMicroservices.TaskExecutor.Api.Models
{
    public class CreateRequestOrderReportOperationModel
    {
        /// <summary>
        /// Date from which the report will be requested
        /// </summary>
        public DateTime FromDate { get; set; }
        /// <summary>
        /// Date up to which the report will be requested.
        /// Cannot be greater than current date
        /// </summary>
        public DateTime ToDate { get; set; }
        /// <summary>
        /// Marketplace Code used to determine EBay marketplace which will be used to generate the report
        /// </summary>
        public string Marketplace { get; set; }
    }
}